#!/bin/bash

if [ $# -eq 1 ]
then
	if [ -r $1 ]
	then
		cat $1
	else
		echo "Create $1"
		touch $1
	fi
else
	echo "Give Filename"
fi

