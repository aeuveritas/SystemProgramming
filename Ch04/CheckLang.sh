#!/bin/sh
for variable in $*
do
	if [ $variable = java ]
	then
		echo "$variable is not supported"
		continue
	elif [ $variable = quit ]
	then
		echo "Quit"
		break
	else
		echo "$variable is supported"
	fi
	echo "Next Language"
done

