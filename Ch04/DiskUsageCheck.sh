#!/bin/bash

usage=$(df $1 | /usr/bin/awk ' \
				{ rem = 0 } { n += 1 } { a = $3 } { b = $4 } \
				n==2 { rem = int(a/(a+b) * 100); print rem } \
				END { }'
)

if [ $usage -ge 90 ] 
then
	echo "DISK($usage) - BAD"
elif [ $usage -ge 70 ]
then
	echo "DISK($usage) - WARNING"
else
	echo "DISK($usage) - GOOD"
fi

