#!/bin/bash

withoutArg()
{
	echo "Run without argument"
}
withArg()
{
	echo "Run with argument"

	while [ $# -gt 0 ]
	do
		echo "Func with $1"
		shift
	done
}

withoutArg
withArg Karl Yeonghun Jeong

