#!/bin/bash

commandName=0
commandIption=0
executed=0

while [ $# -gt 0 ]
do
	case $1 in
	-*)
		commandOption=$1 ;
		executed=2;
		shift ;;
	*)
		if [ $executed = 0 ]
		then
			commandName=$1
			executed=1
			shift
			if [ $# = 0 ]
			then
				$commandName
			fi
		elif [ $executed = 1 ]
		then
			$commandName
			commandName=$1
			shift
			if [ $# = 0 ]
			then
				$commandName
			fi
		fi ;;
	esac
	if [ $executed = 2 ]
	then
		$commandName $commandOption
		executed=0
		echo ""
	fi
done

